package com.jgeraldo.jobsitytvserieschallenge.series;

import com.jgeraldo.jobsitytvserieschallenge.common.BaseObservable;
import com.jgeraldo.jobsitytvserieschallenge.networking.TvMazeCloudApiInterface;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.snackbarhelper.SnackbarHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FetchSeriesListUseCase extends BaseObservable<FetchSeriesListUseCase.Listener> {

    public interface Listener {
        void onSeriesFetched(List<SeriesSchema> seriesSchemas, boolean loadMore);

        void onSeriesFetchFailed(String errorMessage);
    }

    private final TvMazeCloudApiInterface mTvMazeApi;

    private final SnackbarHelper mSnackbarHelper;

    public FetchSeriesListUseCase(TvMazeCloudApiInterface tvMazeCloudApi, SnackbarHelper snackbarHelper) {
        mTvMazeApi = tvMazeCloudApi;
        mSnackbarHelper = snackbarHelper;
    }

    public void fetchSeriesAndNotify(int page, boolean loadMore) {
        mTvMazeApi.getSeries(page)
                .enqueue(new Callback<List<SeriesSchema>>() {
                    @Override
                    public void onResponse(Call<List<SeriesSchema>> call, Response<List<SeriesSchema>> response) {
                        if (response.isSuccessful()) {
                            notifySuccess(response.body(), loadMore);
                        } else {
                            notifyFailure(mSnackbarHelper.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onFailure(Call<List<SeriesSchema>> call, Throwable t) {
                        notifyFailure(mSnackbarHelper.getErrorStringIdByException(t));
                    }
                });
    }

    private void notifyFailure(String errorMessage) {
        for (Listener listener : getListeners()) {
            listener.onSeriesFetchFailed(errorMessage);
        }
    }

    private void notifySuccess(List<SeriesSchema> seriesSchemas, boolean loadMore) {
        for (Listener listener : getListeners()) {
            listener.onSeriesFetched(seriesSchemas, loadMore);
        }
    }
}
