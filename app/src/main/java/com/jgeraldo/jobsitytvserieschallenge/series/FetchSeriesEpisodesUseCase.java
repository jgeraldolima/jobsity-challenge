package com.jgeraldo.jobsitytvserieschallenge.series;

import com.jgeraldo.jobsitytvserieschallenge.common.BaseObservable;
import com.jgeraldo.jobsitytvserieschallenge.networking.TvMazeCloudApiInterface;
import com.jgeraldo.jobsitytvserieschallenge.networking.episode.EpisodeSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.snackbarhelper.SnackbarHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FetchSeriesEpisodesUseCase extends BaseObservable<FetchSeriesEpisodesUseCase.Listener> {

    public interface Listener {
        void onEpisodesFetched(List<EpisodeSchema> episodeSchemas);

        void onEpisodesFetchFailed(String errorMessage);
    }

    private final TvMazeCloudApiInterface mTvMazeApi;

    private final SnackbarHelper mSnackbarHelper;

    public FetchSeriesEpisodesUseCase(TvMazeCloudApiInterface tvMazeCloudApi, SnackbarHelper snackbarHelper) {
        mTvMazeApi = tvMazeCloudApi;
        mSnackbarHelper = snackbarHelper;
    }

    public void fetchEpisodesAndNotify(int seriesId) {
        mTvMazeApi.getEpisodes(seriesId)
                .enqueue(new Callback<List<EpisodeSchema>>() {
                    @Override
                    public void onResponse(Call<List<EpisodeSchema>> call, Response<List<EpisodeSchema>> response) {
                        if (response.isSuccessful()) {
                            notifySuccess(response.body());
                        } else {
                            notifyFailure(mSnackbarHelper.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onFailure(Call<List<EpisodeSchema>> call, Throwable t) {
                        notifyFailure(mSnackbarHelper.getErrorStringIdByException(t));
                    }
                });
    }

    private void notifyFailure(String errorMessage) {
        for (Listener listener : getListeners()) {
            listener.onEpisodesFetchFailed(errorMessage);
        }
    }

    private void notifySuccess(List<EpisodeSchema> episodeSchemas) {
        for (Listener listener : getListeners()) {
            listener.onEpisodesFetched(episodeSchemas);
        }
    }
}
