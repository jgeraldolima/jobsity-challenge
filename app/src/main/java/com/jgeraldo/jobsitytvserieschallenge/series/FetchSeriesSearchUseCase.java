package com.jgeraldo.jobsitytvserieschallenge.series;

import com.jgeraldo.jobsitytvserieschallenge.common.BaseObservable;
import com.jgeraldo.jobsitytvserieschallenge.networking.TvMazeCloudApiInterface;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SearchSeriesResultSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.snackbarhelper.SnackbarHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FetchSeriesSearchUseCase extends BaseObservable<FetchSeriesSearchUseCase.Listener> {

    public interface Listener {
        void onSearchedSeriesFetched(List<SearchSeriesResultSchema> seriesResultSchemas);
        void onSearchSeriesFetchFailed(String errorMessage);
    }

    private final TvMazeCloudApiInterface mTvMazeApi;

    private final SnackbarHelper mSnackbarHelper;

    public FetchSeriesSearchUseCase(TvMazeCloudApiInterface tvMazeCloudApi, SnackbarHelper snackbarHelper) {
        mTvMazeApi = tvMazeCloudApi;
        mSnackbarHelper = snackbarHelper;
    }

    public void searchSeriesAndNotify(String query) {
        mTvMazeApi.getSeriesByName(query)
                .enqueue(new Callback<List<SearchSeriesResultSchema>>() {
                    @Override
                    public void onResponse(Call<List<SearchSeriesResultSchema>> call, Response<List<SearchSeriesResultSchema>> response) {
                        if (response.isSuccessful()) {
                            notifySuccess(response.body());
                        } else {
                            notifyFailure(mSnackbarHelper.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onFailure(Call<List<SearchSeriesResultSchema>> call, Throwable t) {
                        notifyFailure(mSnackbarHelper.getErrorStringIdByException(t));
                    }
                } );
    }

    private void notifyFailure(String errorMessage) {
        for (Listener listener : getListeners()) {
            listener.onSearchSeriesFetchFailed(errorMessage);
        }
    }

    private void notifySuccess(List<SearchSeriesResultSchema> seriesSchemas) {
        for (Listener listener : getListeners()) {
            listener.onSearchedSeriesFetched(seriesSchemas);
        }
    }
}
