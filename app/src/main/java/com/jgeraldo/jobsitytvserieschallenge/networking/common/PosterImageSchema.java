package com.jgeraldo.jobsitytvserieschallenge.networking.common;

import com.google.gson.annotations.SerializedName;
import com.jgeraldo.jobsitytvserieschallenge.common.constants.ApiModelsConstants;

import java.io.Serializable;

public class PosterImageSchema implements Serializable {

    @SerializedName(ApiModelsConstants.POSTER_MEDIUM_URL_PARAM)
    private String mediumImageUrl;

    @SerializedName(ApiModelsConstants.POSTER_ORIGINAL_URL_PARAM)
    private String originalImageUrl;

    public PosterImageSchema(String mediumImageUrl, String originalImageUrl) {
        this.mediumImageUrl = mediumImageUrl;
        this.originalImageUrl = originalImageUrl;
    }

    public String getMediumImageUrl() {
        return mediumImageUrl;
    }

    public void setMediumImageUrl(String mediumImageUrl) {
        this.mediumImageUrl = mediumImageUrl;
    }

    public String getOriginalImageUrl() {
        return originalImageUrl;
    }

    public void setOriginalImageUrl(String originalImageUrl) {
        this.originalImageUrl = originalImageUrl;
    }
}
