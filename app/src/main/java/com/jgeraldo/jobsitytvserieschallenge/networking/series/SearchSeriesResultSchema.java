package com.jgeraldo.jobsitytvserieschallenge.networking.series;

import com.google.gson.annotations.SerializedName;
import com.jgeraldo.jobsitytvserieschallenge.common.constants.ApiModelsConstants;

public class SearchSeriesResultSchema {

    @SerializedName(ApiModelsConstants.SEARCH_SCORE_PARAM)
    private float score;

    @SerializedName(ApiModelsConstants.SEARCH_SERIES_PARAM)
    private SeriesSchema seriesSchema;

    public SearchSeriesResultSchema(float score, SeriesSchema seriesSchema) {
        this.score = score;
        this.seriesSchema = seriesSchema;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public SeriesSchema getSeries() {
        return seriesSchema;
    }

    public void setSeries(SeriesSchema seriesSchema) {
        this.seriesSchema = seriesSchema;
    }
}
