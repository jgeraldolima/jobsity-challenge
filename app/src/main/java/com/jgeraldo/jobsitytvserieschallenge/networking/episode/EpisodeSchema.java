package com.jgeraldo.jobsitytvserieschallenge.networking.episode;

import com.google.gson.annotations.SerializedName;
import com.jgeraldo.jobsitytvserieschallenge.common.constants.ApiModelsConstants;
import com.jgeraldo.jobsitytvserieschallenge.networking.common.PosterImageSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.common.RatingSchema;

import java.io.Serializable;

public class EpisodeSchema implements Serializable {

    @SerializedName(ApiModelsConstants.EPISODE_ID_PARAM)
    private int id;

    @SerializedName(ApiModelsConstants.EPISODE_NUMBER_PARAM)
    private int number;

    @SerializedName(ApiModelsConstants.EPISODE_NAME_PARAM)
    private String name;

    @SerializedName(ApiModelsConstants.EPISODE_SEASON_PARAM)
    private int season;

    @SerializedName(ApiModelsConstants.EPISODE_POSTER_IMAGE_PARAM)
    private PosterImageSchema posterImageSchema;

    @SerializedName(ApiModelsConstants.EPISODE_RATING_PARAM)
    private RatingSchema ratingSchema;

    @SerializedName(ApiModelsConstants.EPISODE_SUMMARY_PARAM)
    private String summary; //html format

    @SerializedName(ApiModelsConstants.EPISODE_URL_PARAM)
    private String url;

    public EpisodeSchema(int id, int number, String name, int season, PosterImageSchema posterImageSchema,
                         RatingSchema ratingSchema, String summary, String url) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.season = season;
        this.posterImageSchema = posterImageSchema;
        this.ratingSchema = ratingSchema;
        this.summary = summary;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeason() {
        return season;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public PosterImageSchema getPosterImage() {
        return posterImageSchema;
    }

    public void setPosterImage(PosterImageSchema posterImageSchema) {
        this.posterImageSchema = posterImageSchema;
    }

    public RatingSchema getRating() {
        return ratingSchema;
    }

    public void setRating(RatingSchema ratingSchema) {
        this.ratingSchema = ratingSchema;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
