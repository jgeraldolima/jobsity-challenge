package com.jgeraldo.jobsitytvserieschallenge.networking.common;

import com.google.gson.annotations.SerializedName;
import com.jgeraldo.jobsitytvserieschallenge.common.constants.ApiModelsConstants;

public class TvMazeErrorResponseSchema {

    @SerializedName(ApiModelsConstants.ERROR_CODE_PARAM)
    private int code;

    @SerializedName(ApiModelsConstants.ERROR_MESSAGE_PARAM)
    private String message;

    @SerializedName(ApiModelsConstants.ERROR_NAME_PARAM)
    private String name;

    @SerializedName(ApiModelsConstants.ERROR_STATUS_PARAM)
    private int status;

    public TvMazeErrorResponseSchema() {
    }

    public TvMazeErrorResponseSchema(int code, String message, String name, int status) {
        this.code = code;
        this.message = message;
        this.name = name;
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
