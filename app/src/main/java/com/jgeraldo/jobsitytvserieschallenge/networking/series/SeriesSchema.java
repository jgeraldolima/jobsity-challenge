package com.jgeraldo.jobsitytvserieschallenge.networking.series;

import com.google.gson.annotations.SerializedName;
import com.jgeraldo.jobsitytvserieschallenge.common.constants.ApiModelsConstants;
import com.jgeraldo.jobsitytvserieschallenge.networking.common.PosterImageSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.common.RatingSchema;

import java.io.Serializable;
import java.util.List;

public class SeriesSchema implements Serializable {

    @SerializedName(ApiModelsConstants.SERIES_ID_PARAM)
    private int id;

    @SerializedName(ApiModelsConstants.SERIES_NAME_PARAM)
    private String name;

    @SerializedName(ApiModelsConstants.SERIES_POSTER_IMAGE_PARAM)
    private PosterImageSchema posterImageSchema;

    @SerializedName(ApiModelsConstants.SERIES_RATING_PARAM)
    private RatingSchema ratingSchema;

    @SerializedName(ApiModelsConstants.SERIES_SCHEDULE_PARAM)
    private SeriesScheduleSchema schedule;

    @SerializedName(ApiModelsConstants.SERIES_GENRES_PARAM)
    private List<String> genres;

    @SerializedName(ApiModelsConstants.SERIES_SUMMARY_PARAM)
    private String summary; //html format

    @SerializedName(ApiModelsConstants.SERIES_URL_PARAM)
    private String url;

    public SeriesSchema(int id, String name, PosterImageSchema posterImageSchema, RatingSchema ratingSchema,
                        SeriesScheduleSchema schedule, List<String> genres, String summary, String url) {
        this.id = id;
        this.name = name;
        this.posterImageSchema = posterImageSchema;
        this.ratingSchema = ratingSchema;
        this.schedule = schedule;
        this.genres = genres;
        this.summary = summary;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PosterImageSchema getPosterImage() {
        return posterImageSchema;
    }

    public void setPosterImage(PosterImageSchema posterImageSchema) {
        this.posterImageSchema = posterImageSchema;
    }

    public RatingSchema getRating() {
        return ratingSchema;
    }

    public void setRating(RatingSchema ratingSchema) {
        this.ratingSchema = ratingSchema;
    }

    public SeriesScheduleSchema getSchedule() {
        return schedule;
    }

    public void setSchedule(SeriesScheduleSchema schedule) {
        this.schedule = schedule;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
