package com.jgeraldo.jobsitytvserieschallenge.networking;

import com.jgeraldo.jobsitytvserieschallenge.common.constants.ApiConstants;
import com.jgeraldo.jobsitytvserieschallenge.networking.episode.EpisodeSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SearchSeriesResultSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TvMazeCloudApiInterface {

    // SHOWS
    @GET(ApiConstants.SERIES_BASE_ENDPOINT)
    Call<List<SeriesSchema>> getSeries(@Query(ApiConstants.PAGE_PARAM) int page);

    @GET(ApiConstants.SERIES_SEARCH_ENDPOINT)
    Call<List<SearchSeriesResultSchema>> getSeriesByName(@Query(ApiConstants.QUERY_PARAM) String query);

    // EPISODES
    @GET(ApiConstants.EPISODES_BASE_ENDPOINT)
    Call<List<EpisodeSchema>> getEpisodes(@Path(ApiConstants.SERIES_ID_PARAM) int showId);

}
