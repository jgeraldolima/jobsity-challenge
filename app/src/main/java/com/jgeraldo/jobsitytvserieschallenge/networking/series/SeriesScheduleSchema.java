package com.jgeraldo.jobsitytvserieschallenge.networking.series;

import com.google.gson.annotations.SerializedName;
import com.jgeraldo.jobsitytvserieschallenge.common.constants.ApiModelsConstants;

import java.io.Serializable;
import java.util.List;

public class SeriesScheduleSchema implements Serializable {

    @SerializedName(ApiModelsConstants.SERIES_SCHEDULE_TIME_PARAM)
    private String time; // hh:mm 24h format

    @SerializedName(ApiModelsConstants.SERIES_SCHEDULE_DAYS_PARAM)
    private List<String> weekDays;

    public SeriesScheduleSchema(String time, List<String> weekDays) {
        this.time = time;
        this.weekDays = weekDays;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<String> getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(List<String> weekDays) {
        this.weekDays = weekDays;
    }
}
