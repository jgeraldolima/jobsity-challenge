package com.jgeraldo.jobsitytvserieschallenge.networking.common;

import com.google.gson.annotations.SerializedName;
import com.jgeraldo.jobsitytvserieschallenge.common.constants.ApiModelsConstants;

import java.io.Serializable;

public class RatingSchema implements Serializable {

    @SerializedName(ApiModelsConstants.RATING_AVERAGE_PARAM)
    private Float average;

    public RatingSchema(Float average) {
        this.average = average;
    }

    public Float getAverage() {
        return average;
    }

    public void setAverage(Float average) {
        this.average = average;
    }
}
