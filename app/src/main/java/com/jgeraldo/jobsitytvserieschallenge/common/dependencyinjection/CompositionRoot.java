package com.jgeraldo.jobsitytvserieschallenge.common.dependencyinjection;

import com.jgeraldo.jobsitytvserieschallenge.common.constants.ApiConstants;
import com.jgeraldo.jobsitytvserieschallenge.networking.TvMazeCloudApiInterface;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CompositionRoot {

    private Retrofit.Builder mRetrofit;

    private static TvMazeCloudApiInterface apiService;

    private Retrofit.Builder getRetrofit() {
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(ApiConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());
        }
        return mRetrofit;
    }

    public TvMazeCloudApiInterface getTvMazeApi() {
        if (apiService == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(ApiConstants.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(ApiConstants.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(ApiConstants.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                    .build();

            getRetrofit().client(okHttpClient);
            Retrofit retrofit = mRetrofit.build();
            apiService = retrofit.create(TvMazeCloudApiInterface.class);
        }
        return apiService;
    }

    // could have something like a DialogsEventBus too, etc
}
