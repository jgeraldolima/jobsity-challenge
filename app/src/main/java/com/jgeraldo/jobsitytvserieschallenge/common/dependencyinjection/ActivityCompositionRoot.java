package com.jgeraldo.jobsitytvserieschallenge.common.dependencyinjection;

import androidx.fragment.app.FragmentActivity;

import com.jgeraldo.jobsitytvserieschallenge.networking.TvMazeCloudApiInterface;

public class ActivityCompositionRoot {

    private final CompositionRoot mCompositionRoot;

    private final FragmentActivity mActivity;

    public ActivityCompositionRoot(CompositionRoot compositionRoot, FragmentActivity activity) {
        mCompositionRoot = compositionRoot;
        mActivity = activity;
    }

    public FragmentActivity getActivity() {
        return mActivity;
    }

    public TvMazeCloudApiInterface getTvMazeApi() {
        return mCompositionRoot.getTvMazeApi();
    }

    // Dialog event bus, Permissions helper, etc...
}
