package com.jgeraldo.jobsitytvserieschallenge.common.constants;

public class Constants {

    // GENERAL
    public static final String APP_TAG = "JobsityTvSeries";

    public static final String SPACE = " ";

    public static final String DASH = "-";

    public static final String SERIES_SCHEDULE_WEEKDAYS_SEPARATOR = ", ";

    public static final int ERROR_DIALOG_DEFAULT_DELAY = 2000;

    // PERMISSION
    public static final int STORAGE_PERMISSION_RESULT = 999;

    // EXTRAS
    public static final String EXTRA_SERIES_ITEM = "EXTRA_SERIES_ITEM";

    // IDENTIFIER AND DEF
    public static final String DEF_STRING = "string";
}
