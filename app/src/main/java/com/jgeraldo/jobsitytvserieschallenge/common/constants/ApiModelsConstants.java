package com.jgeraldo.jobsitytvserieschallenge.common.constants;

public class ApiModelsConstants {

    // SERIES
    public static final String SERIES_ID_PARAM = "id";

    public static final String SERIES_NAME_PARAM = "name";

    public static final String SERIES_POSTER_IMAGE_PARAM = "image";

    public static final String SERIES_RATING_PARAM = "rating";

    public static final String SERIES_SCHEDULE_PARAM = "schedule";

    public static final String SERIES_GENRES_PARAM = "genres";

    public static final String SERIES_SUMMARY_PARAM = "summary";

    public static final String SERIES_URL_PARAM = "url";

    // RATING
    public static final String RATING_AVERAGE_PARAM = "average";

    // SERIES SCHEDULE
    public static final String SERIES_SCHEDULE_TIME_PARAM = "time";

    public static final String SERIES_SCHEDULE_DAYS_PARAM = "days";

    // POSTER IMAGE
    public static final String POSTER_MEDIUM_URL_PARAM = "medium";

    public static final String POSTER_ORIGINAL_URL_PARAM = "original";

    // EPISODE
    public static final String EPISODE_ID_PARAM = "id";

    public static final String EPISODE_NUMBER_PARAM = "number";

    public static final String EPISODE_NAME_PARAM = "name";

    public static final String EPISODE_SEASON_PARAM = "season";

    public static final String EPISODE_POSTER_IMAGE_PARAM = "image";

    public static final String EPISODE_RATING_PARAM = "rating";

    public static final String EPISODE_SUMMARY_PARAM = "summary";

    public static final String EPISODE_URL_PARAM = "url";

    // SEARCH
    public static final String SEARCH_SCORE_PARAM = "score";

    public static final String SEARCH_SERIES_PARAM = "show";


    // ERROR RESPONSE BODY
    public static final String ERROR_CODE_PARAM = "code";

    public static final String ERROR_MESSAGE_PARAM = "message";

    public static final String ERROR_NAME_PARAM = "name";

    public static final String ERROR_STATUS_PARAM = "status";
}