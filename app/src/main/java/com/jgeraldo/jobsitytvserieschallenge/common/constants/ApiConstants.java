package com.jgeraldo.jobsitytvserieschallenge.common.constants;

public class ApiConstants {

    public static final String BASE_URL = "https://api.tvmaze.com"; // TvMaze Cloud

    /* PARAMS */
    public static final String PAGE_PARAM = "page";

    public static final String QUERY_PARAM = "q";

    public static final String SERIES_ID_PARAM = "seriesId";

    public static final int DEFAULT_TIMEOUT = 15;

    /* SEARCH */
    private static final String SEARCH_PREFIX = "/search";

    /* SERIES */
    private static final String SERIES_PREFIX = "/shows";

    public static final String SERIES_BASE_ENDPOINT = SERIES_PREFIX;

    public static final String SERIES_SEARCH_ENDPOINT = SEARCH_PREFIX + SERIES_PREFIX;

    /* SHOWS */
    private static final String EPISODES_PREFIX = "/{seriesId}/episodes";

    public static final String EPISODES_BASE_ENDPOINT = SERIES_PREFIX + EPISODES_PREFIX;
}
