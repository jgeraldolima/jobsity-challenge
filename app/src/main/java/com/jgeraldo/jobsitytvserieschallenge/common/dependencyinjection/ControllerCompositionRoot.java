package com.jgeraldo.jobsitytvserieschallenge.common.dependencyinjection;

import android.content.Context;
import android.view.LayoutInflater;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.jgeraldo.jobsitytvserieschallenge.networking.TvMazeCloudApiInterface;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.ViewMvcFactory;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.fragmentframehelper.FragmentFrameHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.fragmentframehelper.FragmentFrameWrapper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.screensnavigator.ScreensNavigator;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.snackbarhelper.SnackbarHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.splash.SplashController;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.utilshelper.UtilsHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.seriesdetails.SeriesDetailController;
import com.jgeraldo.jobsitytvserieschallenge.screens.serieslist.SeriesListController;
import com.jgeraldo.jobsitytvserieschallenge.series.FetchSeriesEpisodesUseCase;
import com.jgeraldo.jobsitytvserieschallenge.series.FetchSeriesSearchUseCase;
import com.jgeraldo.jobsitytvserieschallenge.series.FetchSeriesListUseCase;

public class ControllerCompositionRoot {

    private final ActivityCompositionRoot mActivityCompositionRoot;

    public ControllerCompositionRoot(ActivityCompositionRoot activityCompositionRoot) {
        mActivityCompositionRoot = activityCompositionRoot;
    }

    private FragmentActivity getActivity() {
        return mActivityCompositionRoot.getActivity();
    }

    private Context getContext() {
        return getActivity();
    }

    private FragmentManager getFragmentManager() {
        return getActivity().getSupportFragmentManager();
    }

    private TvMazeCloudApiInterface getTvMazeApi() {
        return mActivityCompositionRoot.getTvMazeApi();
    }

    private LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(getContext());
    }

    public ViewMvcFactory getViewMvcFactory() {
        return new ViewMvcFactory(getLayoutInflater(), getUtilsHelper());
    }

    public FetchSeriesListUseCase getFetchSeriesListUseCase() {
        return new FetchSeriesListUseCase(getTvMazeApi(), getSnackbarHelper());
    }

    public FetchSeriesSearchUseCase getFetchSearchSeriesUseCase() {
        return new FetchSeriesSearchUseCase(getTvMazeApi(), getSnackbarHelper());
    }

    public FetchSeriesEpisodesUseCase getFetchSeriesEpisodesUseCase() {
        return new FetchSeriesEpisodesUseCase(getTvMazeApi(), getSnackbarHelper());
    }

    public SplashController getSplashController() {
        return new SplashController(
                getActivity(),
                getScreensNavigator()
        );
    }

    public SeriesListController getSeriesListController() {
        return new SeriesListController(
                getActivity(),
                getFetchSeriesListUseCase(),
                getFetchSearchSeriesUseCase(),
                getScreensNavigator(),
                getSnackbarHelper(),
                getUtilsHelper()
        );
    }

    public SeriesDetailController getSeriesDetailController() {
        return new SeriesDetailController(
                getFetchSeriesEpisodesUseCase(),
                getScreensNavigator(),
                getSnackbarHelper()
        );
    }

    public SnackbarHelper getSnackbarHelper() {
        return new SnackbarHelper(getActivity());
    }

    public UtilsHelper getUtilsHelper() {
        return new UtilsHelper(getActivity());
    }

    public ScreensNavigator getScreensNavigator() {
        return new ScreensNavigator(getFragmentFrameHelper(), getUtilsHelper());
    }

    private FragmentFrameHelper getFragmentFrameHelper() {
        return new FragmentFrameHelper(getActivity(), getFragmentFrameWrapper(), getFragmentManager());
    }

    private FragmentFrameWrapper getFragmentFrameWrapper() {
        return (FragmentFrameWrapper) getActivity();
    }

    // Dialog manager, dialog event bus, Navdrawer helper, Permission helper, etc...
}
