package com.jgeraldo.jobsitytvserieschallenge.screens.common.searchviewhelper;

public interface SearchViewHelper {

    boolean onSearchQuerySubmit(String query);

    boolean onSearchQueryChanged(String newQuery);
}
