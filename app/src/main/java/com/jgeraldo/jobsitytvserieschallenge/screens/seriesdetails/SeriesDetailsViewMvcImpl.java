package com.jgeraldo.jobsitytvserieschallenge.screens.seriesdetails;

import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jgeraldo.jobsitytvserieschallenge.R;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.utilshelper.UtilsHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.episodes.SeriesSeasonSpinnerArrayAdapter;
import com.jgeraldo.jobsitytvserieschallenge.networking.common.PosterImageSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.episode.EpisodeSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesScheduleSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.ViewMvcFactory;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.toolbar.ToolbarViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.BaseObservableViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.episodes.EpisodeRecyclerAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.jgeraldo.jobsitytvserieschallenge.common.constants.Constants.DASH;
import static com.jgeraldo.jobsitytvserieschallenge.common.constants.Constants.SERIES_SCHEDULE_WEEKDAYS_SEPARATOR;
import static com.jgeraldo.jobsitytvserieschallenge.common.constants.Constants.SPACE;


public class SeriesDetailsViewMvcImpl extends BaseObservableViewMvc<SeriesDetailsViewMvc.Listener>
        implements SeriesDetailsViewMvc, EpisodeRecyclerAdapter.Listener {

    CollapsingToolbarLayout mCollapsingToolbarLayout;

    Toolbar mToolbar;

    private final ToolbarViewMvc mToolbarViewMvc;

    private final UtilsHelper mUtilsHelper;

    ImageView ivSeriesPoster;

    TextView tvSeriesSchedule;

    TextView tvSeriesGenres;

    TextView tvSeriesSummary;

    ProgressBar pbLoadingEpisodes;

    TextView tvEpisodesEmptyWarning;

    Spinner spSeriesSeason;

    RecyclerView rvSeriesSeasonEpisode;

    FloatingActionButton fabSeriesWebPage;

    private List<Integer> seasons;

    private List<EpisodeSchema> episodeSchemas;

    private List<EpisodeSchema> episodesBySeason = new ArrayList<>();

    private EpisodeRecyclerAdapter episodeItemAdapter;

    private SeriesSeasonSpinnerArrayAdapter seriesSeasonSpinnerArrayAdapter;

    public SeriesDetailsViewMvcImpl(LayoutInflater inflater, ViewGroup parent,
                                    ViewMvcFactory viewMvcFactory, UtilsHelper utilsHelper) {
        setRootView(inflater.inflate(R.layout.fragment_item_detail, parent, false));

        ivSeriesPoster = findViewById(R.id.iv_ctl_series_poster);
        tvSeriesSchedule = findViewById(R.id.tv_series_schedule);
        tvSeriesGenres = findViewById(R.id.tv_series_genres);
        tvSeriesSummary = findViewById(R.id.tv_series_summary);
        pbLoadingEpisodes = findViewById(R.id.pb_loading_episodes);
        tvEpisodesEmptyWarning = findViewById(R.id.tv_episodes_empty_warning);
        spSeriesSeason = findViewById(R.id.sp_series_season);

        rvSeriesSeasonEpisode = findViewById(R.id.rv_series_season_episode);
        episodeItemAdapter = new EpisodeRecyclerAdapter(this, viewMvcFactory);

        fabSeriesWebPage = findViewById(R.id.fab_series_web_page);

        mToolbar = findViewById(R.id.toolbar);
        mToolbarViewMvc = viewMvcFactory.getToolbarViewMvc(mToolbar);

        mUtilsHelper = utilsHelper;

        mCollapsingToolbarLayout = findViewById(R.id.toolbar_layout);

        initToolbar();
        setupEpisodesRecyclerView();
    }

    private void initToolbar() {
        mToolbarViewMvc.setTitle(getString(R.string.title_item_detail));
        mToolbar.addView(mToolbarViewMvc.getRootView());
        mToolbarViewMvc.enableUpButtonAndListen(() -> {
            for (Listener listener : getListeners()) {
                listener.onNavigateUpClicked();
            }
        });
    }

    @Override
    public void bindSeries(SeriesSchema seriesSchema) {
        setupToolbarLayout(seriesSchema);
        setupFab(seriesSchema);

        tvSeriesSchedule.setText(formatSchedule(seriesSchema));
        tvSeriesGenres.setText(mUtilsHelper.getSeriesGenres(seriesSchema));
        tvSeriesSummary.setText(mUtilsHelper.fromHtml(seriesSchema.getSummary()));
    }

    private void setupToolbarLayout(SeriesSchema series) {
        if (series != null) {
            PosterImageSchema poster = series.getPosterImage();
            if (poster != null) {
                Glide.with(getContext())
                        .load(poster.getOriginalImageUrl())
                        .apply(new RequestOptions().error(R.drawable.ic_baseline_movie_24))
                        .into(ivSeriesPoster);
            }

            if (mCollapsingToolbarLayout != null) {
                mCollapsingToolbarLayout.setTitle(series.getName());

                final Typeface segoeUIFont = ResourcesCompat.getFont(getContext(), R.font.segoe_ui);
                mCollapsingToolbarLayout.setCollapsedTitleTypeface(segoeUIFont);
                mCollapsingToolbarLayout.setExpandedTitleTypeface(segoeUIFont);
            }
        }
    }

    private void setupFab(SeriesSchema seriesSchema) {
        String seriesWebPageUrl = seriesSchema.getUrl();
        if (TextUtils.isEmpty(seriesWebPageUrl)) {
            fabSeriesWebPage.setVisibility(View.INVISIBLE);
        } else {
            fabSeriesWebPage.setOnClickListener(v -> mUtilsHelper.openUrl(seriesWebPageUrl));
        }
    }

    private String formatSchedule(SeriesSchema seriesSchema) {
        //TODO: check which days it happens so we can define short strings like "everyday", "weekends", etc
        SeriesScheduleSchema schedule = seriesSchema.getSchedule();

        if (schedule != null) {
            List<String> weekdays = schedule.getWeekDays();

            if (weekdays != null && weekdays.size() > 0) {
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < weekdays.size(); i++) {
                    String weekday = weekdays.get(i);

                    if (weekdays.size() > 1 && i == weekdays.size() - 1) {
                        sb.append(getString(R.string.and)).append(SPACE);
                    }

                    sb.append(weekday).append("s");

                    if (i < weekdays.size() - 1) {
                        sb.append(SERIES_SCHEDULE_WEEKDAYS_SEPARATOR);
                    }
                }

                String time = schedule.getTime();
                if (!TextUtils.isEmpty(time)) {
                    sb.append(SPACE).append(getString(R.string.at_time)).append(SPACE);
                    sb.append(time).append("h");
                }

                return sb.toString();
            } else {
                return DASH;
            }
        } else {
            return DASH;
        }
    }

    @Override
    public void bindEpisodesList(List<EpisodeSchema> episodesList) {
        if (episodesList != null && episodesList.size() > 0) {
            episodeSchemas = episodesList;
            setupSeasonsSpinner();
        } else {
            showNoEpisodesFound();
        }
        hideProgressIndication();
    }

    private void setupSeasonsSpinner() {
        extractSeasons();
        seriesSeasonSpinnerArrayAdapter = new SeriesSeasonSpinnerArrayAdapter(getContext(), seasons);
        seriesSeasonSpinnerArrayAdapter.setDropDownViewResource(R.layout.item_spinner);

        spSeriesSeason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                filterEpisodesBySeason(seasons.get(position));
                episodeItemAdapter.bindEpisodesList(episodesBySeason);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // no op
            }
        });
        spSeriesSeason.setAdapter(seriesSeasonSpinnerArrayAdapter);
        spSeriesSeason.setSelection(0);
    }


    private void setupEpisodesRecyclerView() {
        rvSeriesSeasonEpisode.setAdapter(episodeItemAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvSeriesSeasonEpisode.setLayoutManager(layoutManager);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                getContext(), layoutManager.getOrientation());
        mDividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.episode_list_divider));
        rvSeriesSeasonEpisode.addItemDecoration(mDividerItemDecoration);
    }

    private void extractSeasons() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            seasons = episodeSchemas.stream().map(EpisodeSchema::getSeason).distinct().sorted().collect(Collectors.toList());
        } else {
            Set<Integer> uniqueSeasons = new HashSet<>();
            for (EpisodeSchema ep : episodeSchemas) {
                uniqueSeasons.add(ep.getSeason());
            }
            seasons = new ArrayList<>(uniqueSeasons);
            Collections.sort(seasons);
        }
    }

    private void filterEpisodesBySeason(int season) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            episodesBySeason = episodeSchemas.stream().filter(e -> e.getSeason() == season).collect(Collectors.toList());
        } else {
            episodesBySeason.clear();
            for (EpisodeSchema ep : episodeSchemas) {
                if (ep.getSeason() == season) {
                    episodesBySeason.add(ep);
                }
            }
        }
    }

    @Override
    public void showNoEpisodesFound() {
        pbLoadingEpisodes.setVisibility(View.GONE);
        spSeriesSeason.setVisibility(View.GONE);
        rvSeriesSeasonEpisode.setVisibility(View.GONE);
        tvEpisodesEmptyWarning.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressIndication() {
        setLoading(true);
    }

    @Override
    public void hideProgressIndication() {
        setLoading(false);
    }

    private void setLoading(boolean loading) {
        pbLoadingEpisodes.setVisibility(loading ? View.VISIBLE : View.INVISIBLE);
        spSeriesSeason.setVisibility(loading ? View.INVISIBLE : View.VISIBLE);
        rvSeriesSeasonEpisode.setVisibility(loading ? View.INVISIBLE : View.VISIBLE);
        tvEpisodesEmptyWarning.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onEpisodeClicked(EpisodeSchema episodeSchema) {
        for (Listener listener : getListeners()) {
            listener.onEpisodeClicked(episodeSchema, getContext());
        }
    }
}
