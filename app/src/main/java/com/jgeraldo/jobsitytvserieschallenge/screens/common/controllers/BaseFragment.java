package com.jgeraldo.jobsitytvserieschallenge.screens.common.controllers;

import androidx.fragment.app.Fragment;

import com.jgeraldo.jobsitytvserieschallenge.screens.common.main.MainActivity;
import com.jgeraldo.jobsitytvserieschallenge.common.dependencyinjection.ControllerCompositionRoot;

public class BaseFragment extends Fragment {

    private ControllerCompositionRoot mControllerCompositionRoot;

    protected ControllerCompositionRoot getCompositionRoot() {
        if (mControllerCompositionRoot == null) {
            mControllerCompositionRoot = new ControllerCompositionRoot(
                    ((MainActivity) requireActivity()).getActivityCompositionRoot()
            );
        }
        return mControllerCompositionRoot;
    }
}
