package com.jgeraldo.jobsitytvserieschallenge.screens.common.snackbarhelper;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jgeraldo.jobsitytvserieschallenge.BuildConfig;
import com.jgeraldo.jobsitytvserieschallenge.R;
import com.jgeraldo.jobsitytvserieschallenge.networking.common.TvMazeErrorResponseSchema;
import com.jgeraldo.jobsitytvserieschallenge.common.constants.Constants;

import java.net.UnknownHostException;

import me.crosswall.lib.coverflow.core.Utils;
import okhttp3.ResponseBody;

public class SnackbarHelper {

    private final Context mContext;

    private static final int TOAST_SNACKBAR_WIDTH = 975;

    private static final int TOAST_SNACKBAR_PADDING_BOTTOM = 24;

    public SnackbarHelper(Context context) {
        mContext = context;
    }

    public void showSnackbar(View parentView, String text) {
        showSnackbar(parentView, TOAST_SNACKBAR_WIDTH, TOAST_SNACKBAR_PADDING_BOTTOM, -1, text, false);
    }

    private void showSnackbar(View parentView, int width, int paddingBottom,
                              int textId, String text, boolean isTextId) {
        Snackbar snackbar = Snackbar.make(parentView, "", Snackbar.LENGTH_LONG);
        Snackbar.SnackbarLayout sbView = (Snackbar.SnackbarLayout) snackbar.getView();
        sbView.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));

        // Hide the default text view.
        TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);

        // Inflate custom view.
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View snackView = inflater.inflate(R.layout.snackbar_toast_feedback, null);
        TextView textViewTop = (TextView) snackView.findViewById(R.id.tvSnackbarToastLabel);
        if (isTextId) {
            textViewTop.setText(textId);
        } else {
            textViewTop.setText(text);
        }

        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) sbView.getLayoutParams();
        params.width = width;
        sbView.setLayoutParams(params);

        sbView.setPadding(0, 0, 0, (int) Utils.convertDpToPixel(paddingBottom, mContext));

        sbView.addView(snackView, 0);

        snackbar.show();
    }

    public String getErrorMessage(ResponseBody errorBody) {
        TvMazeErrorResponseSchema error = getError(errorBody);
        Resources resources = mContext.getResources();

        //TODO: map all possible error codes to create strings for each one
        int msgCode = resources.getIdentifier("e" + error.getCode(),
                Constants.DEF_STRING, BuildConfig.APPLICATION_ID);
        return resources.getString(msgCode != 0 ? msgCode : R.string.something_went_wrong);
    }

    private TvMazeErrorResponseSchema getError(ResponseBody errorBody) {
        Gson gson = new GsonBuilder().create();
        TvMazeErrorResponseSchema error = new TvMazeErrorResponseSchema();
        try {
            error = gson.fromJson(errorBody.string(), TvMazeErrorResponseSchema.class);
        } catch (Exception e) {
            // handle failure to read error
        }
        return error;
    }

    public String getErrorStringIdByException(Throwable t) {
        int stringId = R.string.general_error;

        //TODO: maybe need to check other types to give specific message
        if (t instanceof UnknownHostException) {
            stringId = R.string.network_error_msg;
        }

        return mContext.getString(stringId);
    }
}
