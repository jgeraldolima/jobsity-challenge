package com.jgeraldo.jobsitytvserieschallenge.screens.episodes;

import com.jgeraldo.jobsitytvserieschallenge.networking.episode.EpisodeSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.ObservableViewMvc;

public interface EpisodeListItemViewMvc extends ObservableViewMvc<EpisodeListItemViewMvc.Listener> {

    interface Listener {
        void onEpisodeClicked(EpisodeSchema episodeSchema);
    }

    void bindEpisode(EpisodeSchema episodeSchema);
}
