package com.jgeraldo.jobsitytvserieschallenge.screens.serieslist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.jgeraldo.jobsitytvserieschallenge.screens.common.controllers.BaseFragment;

public class SeriesListFragment extends BaseFragment {

    public static Fragment newInstance() {
        return new SeriesListFragment();
    }

    private static final String SAVED_STATE_CONTROLLER = "SAVED_STATE_CONTROLLER";

    private SeriesListController mSeriesListController;

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SeriesListViewMvc viewMvc = getCompositionRoot()
                .getViewMvcFactory()
                .getSeriesListViewMvc(getActivity(), container);

        mSeriesListController = getCompositionRoot().getSeriesListController();
        if (savedInstanceState != null) {
            restoreControllerState(savedInstanceState);
        }
        mSeriesListController.bindView(viewMvc);

        return viewMvc.getRootView();
    }

    private void restoreControllerState(Bundle savedInstanceState) {
        mSeriesListController.restoreSavedState(
                (SeriesListController.SavedState)
                        savedInstanceState.getSerializable(SAVED_STATE_CONTROLLER)
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        mSeriesListController.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mSeriesListController.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_STATE_CONTROLLER, mSeriesListController.getSavedState());
    }
}