package com.jgeraldo.jobsitytvserieschallenge.screens.serieslist;

import com.jgeraldo.jobsitytvserieschallenge.networking.series.SearchSeriesResultSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.searchviewhelper.SearchViewHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.ObservableViewMvc;

import java.util.List;

public interface SeriesListViewMvc extends ObservableViewMvc<SeriesListViewMvc.Listener> {

    void setSearchViewHelper(SearchViewHelper searchViewHelper);

    interface Listener {
        void onLoadMore();
        void onSeriesClicked(SeriesSchema seriesSchema);
    }

    void bindSeriesList(List<SeriesSchema> seriesSchemas, boolean fromLoadMore);

    void bindSeriesList(List<SearchSeriesResultSchema> seriesSchemas);

    void showNoResultsFound();

    void showProgressIndication();

    void hideProgressIndication();

}
