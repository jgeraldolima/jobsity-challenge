package com.jgeraldo.jobsitytvserieschallenge.screens.common.splash;

import android.view.animation.Animation;

import androidx.fragment.app.FragmentActivity;

import com.jgeraldo.jobsitytvserieschallenge.screens.common.main.MainActivity;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.screensnavigator.ScreensNavigator;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.snackbarhelper.SnackbarHelper;
import com.jgeraldo.jobsitytvserieschallenge.series.FetchSeriesListUseCase;
import com.jgeraldo.jobsitytvserieschallenge.series.FetchSeriesSearchUseCase;

public class SplashController implements SplashViewMvc.Listener {

    private final FragmentActivity mFragmentActivity;

    private final ScreensNavigator mScreensNavigator;

    private SplashViewMvc mViewMvc;

    public SplashController(FragmentActivity activity, ScreensNavigator screensNavigator) {
        mFragmentActivity = activity;
        mScreensNavigator = screensNavigator;
    }

    public void bindView(SplashViewMvc viewMvc) {
        mViewMvc = viewMvc;
    }

    public void onStart() {
        mViewMvc.registerListener(this);
        mViewMvc.startAnimation(this);
    }

    public void onStop() {
        mViewMvc.unregisterListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        //no op
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        mScreensNavigator.toMainActivity(mFragmentActivity);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        //no op
    }
}
