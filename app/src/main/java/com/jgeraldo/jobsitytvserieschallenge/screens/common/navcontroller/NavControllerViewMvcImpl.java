package com.jgeraldo.jobsitytvserieschallenge.screens.common.navcontroller;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentContainerView;

import com.jgeraldo.jobsitytvserieschallenge.R;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.BaseAppCompatViewMvc;

public class NavControllerViewMvcImpl extends BaseAppCompatViewMvc implements NavControllerViewMvc {

    private final FragmentContainerView mFragmentContainerView;

    public NavControllerViewMvcImpl(LayoutInflater inflater, @Nullable ViewGroup parent,
                                    AppCompatActivity activity) {
        setAppCompatActivity(activity);
        setRootView(inflater.inflate(R.layout.activity_item_detail, parent, false));
        mFragmentContainerView = findViewById(R.id.nav_host_fragment_item_detail);
    }

    @Override
    public FragmentContainerView getFragmentContainer() {
        return mFragmentContainerView;
    }
}
