package com.jgeraldo.jobsitytvserieschallenge.screens.episodes;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jgeraldo.jobsitytvserieschallenge.R;
import com.jgeraldo.jobsitytvserieschallenge.networking.common.PosterImageSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.episode.EpisodeSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.utilshelper.UtilsHelper;

// could implement some MVC or manager/helper, but we are not using dialogs in any other place in the app
public class EpisodeDetailsDialog {

    private final EpisodeSchema mEpisodeSchema;

    private final Context mContext;

    private final UtilsHelper mUtilsHelper;

    public EpisodeDetailsDialog(EpisodeSchema episode, Context context, UtilsHelper utilsHelper) {
        mEpisodeSchema = episode;
        mContext = context;
        mUtilsHelper = utilsHelper;
    }

    public void openEpisodeDetailsDialog() {
        MaterialDialog episodeDetailsDialog = new MaterialDialog.Builder(mContext)
                .customView(R.layout.episode_details, false)
                .backgroundColorRes(android.R.color.white)
                .build();

        View view = episodeDetailsDialog.getCustomView();

        PosterImageSchema poster = mEpisodeSchema.getPosterImage();
        if (poster != null) {
            Glide.with(mContext)
                    .load(poster.getOriginalImageUrl())
                    .apply(new RequestOptions().error(R.drawable.ic_baseline_movie_24))
                    .into((ImageView) view.findViewById(R.id.iv_episode_poster_details));
        }

        FloatingActionButton fabEpisodeWebPage = view.findViewById(R.id.fab_episode_web_page);
        String episodeWebPageUrl = mEpisodeSchema.getUrl();
        if (TextUtils.isEmpty(episodeWebPageUrl)) {
            fabEpisodeWebPage.setVisibility(View.INVISIBLE);
        } else {
            fabEpisodeWebPage.setOnClickListener(v -> mUtilsHelper.openUrl(episodeWebPageUrl));
        }

        TextView tvEpisodeTitleDetails = view.findViewById(R.id.tv_episode_title_details);
        tvEpisodeTitleDetails.setText(String.format("%s %s - #%s",
                mContext.getString(R.string.series_season_prefix), mEpisodeSchema.getSeason(),
                mEpisodeSchema.getNumber()));

        TextView tvEpisodeNameDetails = view.findViewById(R.id.tv_episode_name_details);
        tvEpisodeNameDetails.setText(mEpisodeSchema.getName());

        TextView tvEpisodeSummaryDetails = view.findViewById(R.id.tv_episode_summary_details);
        tvEpisodeSummaryDetails.setText(mUtilsHelper.fromHtml(mEpisodeSchema.getSummary()));

        episodeDetailsDialog.show();
    }

}
