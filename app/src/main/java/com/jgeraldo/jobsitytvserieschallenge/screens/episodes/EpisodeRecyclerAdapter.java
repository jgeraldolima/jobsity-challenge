package com.jgeraldo.jobsitytvserieschallenge.screens.episodes;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jgeraldo.jobsitytvserieschallenge.networking.episode.EpisodeSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.ViewMvcFactory;

import java.util.ArrayList;
import java.util.List;

public class EpisodeRecyclerAdapter extends RecyclerView.Adapter<EpisodeRecyclerAdapter.MyViewHolder>
        implements EpisodeListItemViewMvc.Listener {

    public interface Listener {
        void onEpisodeClicked(EpisodeSchema episodeSchema);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        private final EpisodeListItemViewMvc mViewMvc;

        public MyViewHolder(EpisodeListItemViewMvc viewMvc) {
            super(viewMvc.getRootView());
            mViewMvc = viewMvc;
        }
    }

    private final Listener mListener;

    private final ViewMvcFactory mViewMvcFactory;

    private List<EpisodeSchema> mEpisodeSchemas = new ArrayList<>();

    public EpisodeRecyclerAdapter(Listener listener, ViewMvcFactory viewMvcFactory) {
        mListener = listener;
        mViewMvcFactory = viewMvcFactory;
    }

    public void bindEpisodesList(List<EpisodeSchema> episodeSchemas) {
        mEpisodeSchemas = new ArrayList<>(episodeSchemas);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        EpisodeListItemViewMvc viewMvc = mViewMvcFactory.getEpisodeListItemViewMvc(parent);
        viewMvc.registerListener(this);
        return new MyViewHolder(viewMvc);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.mViewMvc.bindEpisode(mEpisodeSchemas.get(position));
    }

    @Override
    public int getItemCount() {
        if (mEpisodeSchemas != null) {
            return mEpisodeSchemas.size();
        }
        return 0;
    }

    @Override
    public void onEpisodeClicked(EpisodeSchema episodeSchema) {
        mListener.onEpisodeClicked(episodeSchema);
    }
}
