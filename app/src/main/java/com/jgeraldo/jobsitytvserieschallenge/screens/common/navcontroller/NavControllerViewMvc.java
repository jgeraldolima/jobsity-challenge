package com.jgeraldo.jobsitytvserieschallenge.screens.common.navcontroller;

import androidx.fragment.app.FragmentContainerView;

import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.ViewMvc;

public interface NavControllerViewMvc extends ViewMvc {
    FragmentContainerView getFragmentContainer();
}
