package com.jgeraldo.jobsitytvserieschallenge.screens.seriesdetails;

import android.content.Context;

import com.jgeraldo.jobsitytvserieschallenge.networking.episode.EpisodeSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.ObservableViewMvc;

import java.util.List;

public interface SeriesDetailsViewMvc extends ObservableViewMvc<SeriesDetailsViewMvc.Listener> {

    interface Listener {
        void onNavigateUpClicked();
        void onEpisodeClicked(EpisodeSchema episode, Context context);
    }

    void bindSeries(SeriesSchema series);

    void bindEpisodesList(List<EpisodeSchema> episodesList);

    void showNoEpisodesFound();

    void showProgressIndication();

    void hideProgressIndication();
}
