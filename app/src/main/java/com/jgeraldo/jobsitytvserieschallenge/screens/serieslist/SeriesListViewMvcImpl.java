package com.jgeraldo.jobsitytvserieschallenge.screens.serieslist;

import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jgeraldo.jobsitytvserieschallenge.R;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SearchSeriesResultSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.ViewMvcFactory;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.searchviewhelper.SearchViewHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.toolbar.ToolbarViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.utilshelper.UtilsHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.BaseObservableViewMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SeriesListViewMvcImpl extends BaseObservableViewMvc<SeriesListViewMvc.Listener>
        implements SeriesListViewMvc, SeriesRecyclerAdapter.Listener {

    private final FragmentActivity mFragmentActivity;

    private SearchViewHelper mSearchViewHelper;

    private final Toolbar mToolbar;

    private final ToolbarViewMvc mToolbarViewMvc;

    private final UtilsHelper mUtilsHelper;

    private final SearchView svSeriesByName;

    private final RecyclerView rvSeries;

    private final SeriesRecyclerAdapter mAdapter;

    private final TextView tvSeriesEmptyWarning;

    private final ProgressBar mProgressBar;

    private List<SeriesSchema> seriesSchemas = new ArrayList<>();

    public SeriesListViewMvcImpl(FragmentActivity activity, LayoutInflater inflater,
                                 @Nullable ViewGroup parent, ViewMvcFactory viewMvcFactory,
                                 UtilsHelper utilsHelper) {
        setRootView(inflater.inflate(R.layout.fragment_series_list, parent, false));

        mFragmentActivity = activity;

        mToolbar = findViewById(R.id.toolbar);
        mToolbarViewMvc = viewMvcFactory.getToolbarViewMvc(mToolbar);

        mUtilsHelper = utilsHelper;

        svSeriesByName = findViewById(R.id.sv_series_by_name);

        rvSeries = findViewById(R.id.rv_series);
        mAdapter = new SeriesRecyclerAdapter(this, viewMvcFactory);

        tvSeriesEmptyWarning = findViewById(R.id.tv_series_empty_warning);

        mProgressBar = findViewById(R.id.pb_loading_series);

        initToolbar();
        setupSearchView();
        setupRecyclerView();
    }

    private void initToolbar() {
        mToolbarViewMvc.setTitle(getString(R.string.shows_activity_title));
        mToolbar.addView(mToolbarViewMvc.getRootView());
    }

    private void setupSearchView() {
        TextView tvSearchViewHint = (TextView)
                svSeriesByName.findViewById(androidx.appcompat.R.id.search_src_text);
        final Typeface segoeUIFont = ResourcesCompat.getFont(getContext(), R.font.segoeuisl);
        tvSearchViewHint.setTypeface(segoeUIFont);

        svSeriesByName.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return mSearchViewHelper.onSearchQuerySubmit(query);
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return mSearchViewHelper.onSearchQueryChanged(newText);
            }
        });
    }

    private void setupRecyclerView() {
        rvSeries.setAdapter(mAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvSeries.setLayoutManager(layoutManager);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                getContext(), layoutManager.getOrientation());
        mDividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.list_divider));
        rvSeries.addItemDecoration(mDividerItemDecoration);
    }

    @Override
    public void onLoadMore() {
        for (Listener listener : getListeners()) {
            listener.onLoadMore();
        }
    }

    @Override
    public void onSeriesClicked(SeriesSchema seriesSchema) {
        for (Listener listener : getListeners()) {
            listener.onSeriesClicked(seriesSchema);
        }
    }

    @Override
    public void bindSeriesList(List<SeriesSchema> seriesSchemaFromResponse, boolean fromLoadMore) {
        if (seriesSchemaFromResponse != null && seriesSchemaFromResponse.size() > 0) {
            if (fromLoadMore) {
                seriesSchemas.addAll(seriesSchemaFromResponse);
            } else {
                seriesSchemas = seriesSchemaFromResponse;
            }
            mAdapter.bindSeriesList(seriesSchemas);
        } else {
            showNoResultsFound();
        }
        hideProgressIndication();
    }

    @Override
    public void bindSeriesList(List<SearchSeriesResultSchema> seriesResultSchemas) {
        // come from search
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            seriesSchemas = seriesResultSchemas.stream().map(SearchSeriesResultSchema::getSeries).collect(Collectors.toList());
        } else {
            seriesSchemas.clear();
            for (SearchSeriesResultSchema ssr : seriesResultSchemas) {
                seriesSchemas.add(ssr.getSeries());
            }
        }
        mAdapter.bindSeriesList(seriesSchemas);
        hideProgressIndication();
    }

    @Override
    public void setSearchViewHelper(SearchViewHelper searchViewHelper) {
        mSearchViewHelper = searchViewHelper;
    }

    @Override
    public void showNoResultsFound() {
        mUtilsHelper.closeSoftKeyboard();
        hideProgressIndication();
        svSeriesByName.setVisibility(View.VISIBLE);
        rvSeries.setVisibility(View.GONE);
        tvSeriesEmptyWarning.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressIndication() {
        setLoading(true);
    }

    @Override
    public void hideProgressIndication() {
        setLoading(false);
    }

    private void setLoading(boolean loading) {
        mUtilsHelper.closeSoftKeyboard();
        mProgressBar.setVisibility(loading ? View.VISIBLE : View.INVISIBLE);
        svSeriesByName.setVisibility(loading ? View.INVISIBLE : View.VISIBLE);
        rvSeries.setVisibility(loading ? View.INVISIBLE : View.VISIBLE);
        tvSeriesEmptyWarning.setVisibility(View.INVISIBLE);
    }
}
