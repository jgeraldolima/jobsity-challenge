package com.jgeraldo.jobsitytvserieschallenge.screens.episodes;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jgeraldo.jobsitytvserieschallenge.R;
import com.jgeraldo.jobsitytvserieschallenge.networking.common.PosterImageSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.episode.EpisodeSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.BaseObservableViewMvc;

public class EpisodeListItemViewMvcImpl extends BaseObservableViewMvc<EpisodeListItemViewMvc.Listener>
        implements EpisodeListItemViewMvc {

    private final ImageView ivEpisodePoster;

    private final TextView tvEpisodeTitle;

    private EpisodeSchema mEpisodeSchema;

    public EpisodeListItemViewMvcImpl(LayoutInflater inflater, @Nullable ViewGroup parent) {
        setRootView(inflater.inflate(R.layout.episode_item_content, parent, false));

        ivEpisodePoster = findViewById(R.id.iv_episode_poster);
        tvEpisodeTitle = findViewById(R.id.tv_episode_title);

        getRootView().setOnClickListener(view -> {
            for (Listener listener : getListeners()) {
                listener.onEpisodeClicked(mEpisodeSchema);
            }
        });
    }

    @Override
    public void bindEpisode(EpisodeSchema episodeSchema) {
        mEpisodeSchema = episodeSchema;

        PosterImageSchema poster = episodeSchema.getPosterImage();
        if (poster != null) {
            Glide.with(getContext())
                    .load(poster.getOriginalImageUrl())
                    .apply(new RequestOptions().error(R.drawable.ic_baseline_movie_24))
                    .into(ivEpisodePoster);
        }

        tvEpisodeTitle.setText(String.format("%s - %s", episodeSchema.getNumber(), episodeSchema.getName()));
    }
}
