package com.jgeraldo.jobsitytvserieschallenge.screens.episodes;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jgeraldo.jobsitytvserieschallenge.R;

import java.util.List;

public class SeriesSeasonSpinnerArrayAdapter extends ArrayAdapter<Integer> {

    private static final int VIEW_TYPE_HINT = 1;
    private List<Integer> seasons;

    public SeriesSeasonSpinnerArrayAdapter(@NonNull Context context, @NonNull List<Integer> seasons) {
        super(context, 0, seasons);
        this.seasons = seasons;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_spinner_season_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (getItemViewType(position) == VIEW_TYPE_HINT) {
            viewHolder.tvName.setTextColor(parent.getContext().getResources()
                    .getColor(R.color.teal_200));
        } else {
            viewHolder.tvName.setTextColor(Color.BLACK);
        }

        String prefix = parent.getContext().getString(R.string.series_season_prefix);

        viewHolder.tvName.setText(String.format("%s %s", prefix, seasons.get(position)));
        return convertView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    static class ViewHolder {
        TextView tvName;

        public ViewHolder(View view) {
            tvName = view.findViewById(android.R.id.text1);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return VIEW_TYPE_HINT;
        }
        return super.getItemViewType(position);
    }
}