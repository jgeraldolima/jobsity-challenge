package com.jgeraldo.jobsitytvserieschallenge.screens.common.views;

import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseAppCompatViewMvc extends BaseViewMvc {

    private AppCompatActivity mAppCompatActivity;

    public AppCompatActivity getAppCompatActivity() {
        return mAppCompatActivity;
    }

    protected void setAppCompatActivity(AppCompatActivity appCompatActivity) {
        mAppCompatActivity = appCompatActivity;
    }

}
