package com.jgeraldo.jobsitytvserieschallenge.screens.common.splash;

import android.view.animation.Animation;

import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.ObservableViewMvc;

public interface SplashViewMvc extends ObservableViewMvc<SplashViewMvc.Listener>  {

    interface Listener extends Animation.AnimationListener {
        void onAnimationStart(Animation animation);

        void onAnimationEnd(Animation animation);

        void onAnimationRepeat(Animation animation);
    }

    void startAnimation(Animation.AnimationListener listener);
}
