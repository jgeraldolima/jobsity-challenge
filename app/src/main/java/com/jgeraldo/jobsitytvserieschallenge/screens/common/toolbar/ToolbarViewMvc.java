package com.jgeraldo.jobsitytvserieschallenge.screens.common.toolbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.jgeraldo.jobsitytvserieschallenge.R;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.utilshelper.UtilsHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.BaseViewMvc;

public class ToolbarViewMvc extends BaseViewMvc {

    public interface NavigateUpClickListener {
        void onNavigateUpClicked();
    }

    private final TextView mTxtTitle;

    private final ImageButton mBtnBack;

    private NavigateUpClickListener mNavigateUpClickListener;

    private final UtilsHelper mUtilsHelper;

    public ToolbarViewMvc(LayoutInflater inflater, ViewGroup parent, UtilsHelper utilsHelper) {
        setRootView(inflater.inflate(R.layout.layout_toolbar, parent, false));

        mTxtTitle = findViewById(R.id.txt_toolbar_title);
        mBtnBack = findViewById(R.id.btn_back);
        mBtnBack.setOnClickListener(view -> mNavigateUpClickListener.onNavigateUpClicked());

        mUtilsHelper = utilsHelper;
    }

    public void setTitle(String title) {
        mTxtTitle.setText(title);
    }

    public void enableUpButtonAndListen(NavigateUpClickListener navigateUpClickListener) {
        mNavigateUpClickListener = navigateUpClickListener;

        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mTxtTitle.getLayoutParams();
        layoutParams.setMarginStart((int) mUtilsHelper.convertDpToPixel(16));
        mTxtTitle.setLayoutParams(layoutParams);
        mBtnBack.setVisibility(View.VISIBLE);
    }
}
