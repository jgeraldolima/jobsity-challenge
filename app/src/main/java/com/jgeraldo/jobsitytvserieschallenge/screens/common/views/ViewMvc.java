package com.jgeraldo.jobsitytvserieschallenge.screens.common.views;

import android.view.View;

public interface ViewMvc {
    View getRootView();
}
