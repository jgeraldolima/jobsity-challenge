package com.jgeraldo.jobsitytvserieschallenge.screens.common;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.jgeraldo.jobsitytvserieschallenge.screens.common.navcontroller.NavControllerViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.navcontroller.NavControllerViewMvcImpl;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.splash.SplashViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.splash.SplashViewMvcImpl;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.toolbar.ToolbarViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.utilshelper.UtilsHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.episodes.EpisodeListItemViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.episodes.EpisodeListItemViewMvcImpl;
import com.jgeraldo.jobsitytvserieschallenge.screens.seriesdetails.SeriesDetailsViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.seriesdetails.SeriesDetailsViewMvcImpl;
import com.jgeraldo.jobsitytvserieschallenge.screens.serieslist.SeriesListViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.serieslist.SeriesListViewMvcImpl;
import com.jgeraldo.jobsitytvserieschallenge.screens.serieslist.serieslistitem.SeriesListItemViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.serieslist.serieslistitem.SeriesListItemViewMvcImpl;

public class ViewMvcFactory {

    private final LayoutInflater mLayoutInflater;

    private final UtilsHelper mUtilsHelper;

    public ViewMvcFactory(LayoutInflater layoutInflater, UtilsHelper utilsHelper) {
        mLayoutInflater = layoutInflater;
        mUtilsHelper = utilsHelper;
    }

    public SeriesListViewMvc getSeriesListViewMvc(FragmentActivity activity, @Nullable ViewGroup parent) {
        return new SeriesListViewMvcImpl(activity, mLayoutInflater, parent, this, mUtilsHelper);
    }

    public SeriesListItemViewMvc getSeriesListItemViewMvc(@Nullable ViewGroup parent) {
        return new SeriesListItemViewMvcImpl(mLayoutInflater, parent, mUtilsHelper);
    }

    public SeriesDetailsViewMvc getSeriesDetailViewMvc(@Nullable ViewGroup parent) {
        return new SeriesDetailsViewMvcImpl(mLayoutInflater, parent, this, mUtilsHelper);
    }

    public EpisodeListItemViewMvc getEpisodeListItemViewMvc(@Nullable ViewGroup parent) {
        return new EpisodeListItemViewMvcImpl(mLayoutInflater, parent);
    }

    public NavControllerViewMvc getNavControllerViewMvc(@Nullable ViewGroup parent, AppCompatActivity activity) {
        return new NavControllerViewMvcImpl(mLayoutInflater, parent, activity);
    }

    public ToolbarViewMvc getToolbarViewMvc(@Nullable ViewGroup parent) {
        return new ToolbarViewMvc(mLayoutInflater, parent, mUtilsHelper);
    }

    public SplashViewMvc getSplashViewMvc(@Nullable ViewGroup parent) {
        return new SplashViewMvcImpl(mLayoutInflater, parent);
    }

    // toolbar mvc, navdrawer mvc, etc...
}
