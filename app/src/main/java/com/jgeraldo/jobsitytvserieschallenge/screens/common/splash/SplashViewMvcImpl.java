package com.jgeraldo.jobsitytvserieschallenge.screens.common.splash;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;

import com.jgeraldo.jobsitytvserieschallenge.R;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.BaseObservableViewMvc;

public class SplashViewMvcImpl extends BaseObservableViewMvc<SplashViewMvc.Listener> implements SplashViewMvc {

    ImageView ivSplash;

    public SplashViewMvcImpl(LayoutInflater inflater, ViewGroup parent) {
        setRootView(inflater.inflate(R.layout.activity_splash, parent, false));

        ivSplash = findViewById(R.id.iv_splash);
    }

    @Override
    public void startAnimation(Animation.AnimationListener listener) {
        Animation logoAnim = AnimationUtils.loadAnimation(getContext(), R.anim.alpha_anim);
        Interpolator animInterpolator = new DecelerateInterpolator();
        logoAnim.setInterpolator(animInterpolator);
        logoAnim.setAnimationListener(listener);
        ivSplash.requestLayout();
        ivSplash.setAnimation(logoAnim);
        logoAnim.start();
    }
}
