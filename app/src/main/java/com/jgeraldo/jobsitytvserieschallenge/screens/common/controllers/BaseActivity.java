package com.jgeraldo.jobsitytvserieschallenge.screens.common.controllers;

import androidx.appcompat.app.AppCompatActivity;

import com.jgeraldo.jobsitytvserieschallenge.common.CustomApplication;
import com.jgeraldo.jobsitytvserieschallenge.common.dependencyinjection.ActivityCompositionRoot;
import com.jgeraldo.jobsitytvserieschallenge.common.dependencyinjection.ControllerCompositionRoot;

public class BaseActivity extends AppCompatActivity {

    private ActivityCompositionRoot mActivityCompositionRoot;

    private ControllerCompositionRoot mControllerCompositionRoot;

    public ActivityCompositionRoot getActivityCompositionRoot() {
        if (mActivityCompositionRoot == null) {
            mActivityCompositionRoot = new ActivityCompositionRoot(
                    ((CustomApplication) getApplication()).getCompositionRoot(),
                    this
            );
        }
        return mActivityCompositionRoot;
    }

    protected ControllerCompositionRoot getCompositionRoot() {
        if (mControllerCompositionRoot == null) {
            mControllerCompositionRoot = new ControllerCompositionRoot(getActivityCompositionRoot());
        }
        return mControllerCompositionRoot;
    }

}
