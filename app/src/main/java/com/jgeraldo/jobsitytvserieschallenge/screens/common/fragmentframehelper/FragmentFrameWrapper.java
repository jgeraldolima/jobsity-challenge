package com.jgeraldo.jobsitytvserieschallenge.screens.common.fragmentframehelper;

import android.widget.FrameLayout;

public interface FragmentFrameWrapper {

    FrameLayout getFragmentFrame();
}
