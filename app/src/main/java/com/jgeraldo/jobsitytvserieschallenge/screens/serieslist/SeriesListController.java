package com.jgeraldo.jobsitytvserieschallenge.screens.serieslist;

import android.text.TextUtils;

import androidx.fragment.app.FragmentActivity;

import com.jgeraldo.jobsitytvserieschallenge.networking.series.SearchSeriesResultSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.screensnavigator.ScreensNavigator;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.searchviewhelper.SearchViewHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.snackbarhelper.SnackbarHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.utilshelper.UtilsHelper;
import com.jgeraldo.jobsitytvserieschallenge.series.FetchSeriesSearchUseCase;
import com.jgeraldo.jobsitytvserieschallenge.series.FetchSeriesListUseCase;

import java.io.Serializable;
import java.util.List;

public class SeriesListController implements
        SeriesListViewMvc.Listener,
        FetchSeriesListUseCase.Listener,
        FetchSeriesSearchUseCase.Listener,
        SearchViewHelper {

    private enum ScreenState {
        IDLE, FETCHING_SERIES, SEARCHING_SERIES, SERIES_LIST_SHOWN, NO_RESULTS, REQUEST_ERROR
    }

    private final FragmentActivity mFragmentActivity;

    private final FetchSeriesListUseCase mFetchSeriesListUseCase;

    private final FetchSeriesSearchUseCase mFetchSeriesSearchUseCase;

    private final ScreensNavigator mScreensNavigator;

    private final SnackbarHelper mSnackbarHelper;

    private final UtilsHelper mUtilsHelper;

    private SeriesListViewMvc mViewMvc;

    private ScreenState mScreenState = ScreenState.IDLE;

    private int currentPage = 1;

    private boolean searchMode = false;

    private String lastQuery = "";

    public SeriesListController(FragmentActivity activity, FetchSeriesListUseCase fetchSeriesListUseCase,
                                FetchSeriesSearchUseCase fetchSeriesSearchUseCase,
                                ScreensNavigator screensNavigator, SnackbarHelper snackbarHelper, UtilsHelper utilsHelper) {
        mFragmentActivity = activity;
        mFetchSeriesListUseCase = fetchSeriesListUseCase;
        mFetchSeriesSearchUseCase = fetchSeriesSearchUseCase;
        mScreensNavigator = screensNavigator;
        mSnackbarHelper = snackbarHelper;
        mUtilsHelper = utilsHelper;
    }

    public void bindView(SeriesListViewMvc viewMvc) {
        mViewMvc = viewMvc;
        mViewMvc.setSearchViewHelper(this);
    }

    public SavedState getSavedState() {
        return new SavedState(mScreenState);
    }

    public void restoreSavedState(SavedState savedState) {
        mScreenState = savedState.mScreenState;
    }

    public void onStart() {
        mViewMvc.registerListener(this);
        mFetchSeriesListUseCase.registerListener(this);
        mFetchSeriesSearchUseCase.registerListener(this);

        if (mScreenState != ScreenState.REQUEST_ERROR) {
            fetchSeriesAndNotify(false);
        }
    }

    public void onStop() {
        lastQuery = "";
        mViewMvc.unregisterListener(this);
        mFetchSeriesListUseCase.unregisterListener(this);
        mFetchSeriesSearchUseCase.unregisterListener(this);
    }

    private void fetchSeriesAndNotify(boolean loadMore) {
        if (mScreenState != ScreenState.FETCHING_SERIES && !searchMode) {
            mScreenState = ScreenState.FETCHING_SERIES;
            mViewMvc.showProgressIndication();
            mFetchSeriesListUseCase.fetchSeriesAndNotify(currentPage, loadMore);
        }
    }

    private void searchSeriesAndNotify(String query) {
        searchMode = true;
        mScreenState = ScreenState.FETCHING_SERIES;
        mViewMvc.showProgressIndication();
        mFetchSeriesSearchUseCase.searchSeriesAndNotify(query);
    }

    @Override
    public void onLoadMore() {
        fetchSeriesAndNotify(true);
    }

    @Override
    public void onSeriesClicked(SeriesSchema seriesSchema) {
        mScreensNavigator.toSeriesDetails(seriesSchema);
    }

    @Override
    public boolean onSearchQuerySubmit(String query) {
        if (TextUtils.isEmpty(query)) {
            return false;
        }

        searchSeriesAndNotify(query);
        mUtilsHelper.closeSoftKeyboard();
        return true;
    }

    @Override
    public boolean onSearchQueryChanged(String newQuery) {
        // user erased the input, so we need to get a fresh list again
        if (!TextUtils.isEmpty(lastQuery) && TextUtils.isEmpty(newQuery)) {
            searchMode = false;
            mViewMvc.showProgressIndication();
            currentPage = 1;
            fetchSeriesAndNotify(false);
        }
        lastQuery = newQuery;
        return false;
    }

    @Override
    public void onSeriesFetched(List<SeriesSchema> seriesSchemaFromResponse, boolean loadMore) {
        if (seriesSchemaFromResponse != null && seriesSchemaFromResponse.size() > 0) {
            currentPage++;
            mViewMvc.bindSeriesList(seriesSchemaFromResponse, loadMore);
            mScreenState = ScreenState.SERIES_LIST_SHOWN;
        } else {
            mScreenState = ScreenState.NO_RESULTS;
            mViewMvc.showNoResultsFound();
        }
    }

    @Override
    public void onSeriesFetchFailed(String errorMessage) {
        mScreenState = ScreenState.REQUEST_ERROR;
        mViewMvc.hideProgressIndication();
        mSnackbarHelper.showSnackbar(mViewMvc.getRootView(), errorMessage);
    }

    @Override
    public void onSearchedSeriesFetched(List<SearchSeriesResultSchema> seriesResultSchemas) {
        if (seriesResultSchemas != null && seriesResultSchemas.size() > 0) {
            mScreenState = ScreenState.SERIES_LIST_SHOWN;
            mViewMvc.bindSeriesList(seriesResultSchemas);
        } else {
            mScreenState = ScreenState.NO_RESULTS;
            mViewMvc.showNoResultsFound();
        }
    }

    @Override
    public void onSearchSeriesFetchFailed(String errorMessage) {
        mScreenState = ScreenState.REQUEST_ERROR;
        mViewMvc.hideProgressIndication();
        mSnackbarHelper.showSnackbar(mViewMvc.getRootView(), errorMessage);
    }

    public static class SavedState implements Serializable {
        private final ScreenState mScreenState;

        public SavedState(ScreenState screenState) {
            mScreenState = screenState;
        }
    }
}
