package com.jgeraldo.jobsitytvserieschallenge.screens.serieslist.serieslistitem;

import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.ObservableViewMvc;

public interface SeriesListItemViewMvc extends ObservableViewMvc<SeriesListItemViewMvc.Listener> {

    interface Listener {
        void onSeriesClicked(SeriesSchema seriesSchema);
    }

    void bindSeries(SeriesSchema seriesSchema);
}
