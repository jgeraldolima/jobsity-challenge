package com.jgeraldo.jobsitytvserieschallenge.screens.common.main;

import android.os.Bundle;

import androidx.fragment.app.FragmentContainerView;

import com.jgeraldo.jobsitytvserieschallenge.screens.common.controllers.BaseSupportNavigationUpActivity;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.fragmentframehelper.FragmentFrameWrapper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.navcontroller.NavControllerViewMvc;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.screensnavigator.ScreensNavigator;

public class MainActivity extends BaseSupportNavigationUpActivity implements
        FragmentFrameWrapper {

    private NavControllerViewMvc mViewMvc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScreensNavigator mScreensNavigator = getCompositionRoot().getScreensNavigator();
        mViewMvc = getCompositionRoot().getViewMvcFactory().getNavControllerViewMvc(null, this);
        setContentView(mViewMvc.getRootView());

        if (savedInstanceState == null) {
            mScreensNavigator.toSeriesList();
        }
    }

    @Override
    public FragmentContainerView getFragmentFrame() {
        return mViewMvc.getFragmentContainer();
    }
}