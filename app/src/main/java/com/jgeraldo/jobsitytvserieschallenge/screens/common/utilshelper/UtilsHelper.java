package com.jgeraldo.jobsitytvserieschallenge.screens.common.utilshelper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;

import java.util.List;

import static com.jgeraldo.jobsitytvserieschallenge.common.constants.Constants.DASH;
import static com.jgeraldo.jobsitytvserieschallenge.common.constants.Constants.SERIES_SCHEDULE_WEEKDAYS_SEPARATOR;

public class UtilsHelper {

    private final Activity mActivity;

    public UtilsHelper(Activity activity) {
        mActivity = activity;
    }

    public float convertDpToPixel(float dp) {
        if (dp == 0) {
            return 0;
        }
        return dp * ((float) mActivity.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String source) {
        if (TextUtils.isEmpty(source)) {
            source = DASH;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }

    public void closeSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = mActivity.getCurrentFocus() == null ? new View(mActivity) : mActivity.getCurrentFocus();
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public String getSeriesGenres(SeriesSchema seriesSchema) {
        StringBuilder genresBuilder = new StringBuilder();
        List<String> genres = seriesSchema.getGenres();

        if (genres != null && genres.size() > 0) {
            for (String genre : genres) {
                genresBuilder.append(genre);
                genresBuilder.append(SERIES_SCHEDULE_WEEKDAYS_SEPARATOR);
            }
        }

        if (genresBuilder.length() >= 2) {
            return genresBuilder.substring(0, genresBuilder.length() - 2);
        }
        return DASH;
    }

    public void openUrl(String url) {
        Intent browser = new Intent(Intent.ACTION_VIEW);
        browser.setData(Uri.parse(url));
        mActivity.startActivity(browser);
    }
}
