package com.jgeraldo.jobsitytvserieschallenge.screens.common.screensnavigator;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.FragmentActivity;

import com.jgeraldo.jobsitytvserieschallenge.networking.episode.EpisodeSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.fragmentframehelper.FragmentFrameHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.main.MainActivity;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.utilshelper.UtilsHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.episodes.EpisodeDetailsDialog;
import com.jgeraldo.jobsitytvserieschallenge.screens.seriesdetails.SeriesDetailFragment;
import com.jgeraldo.jobsitytvserieschallenge.screens.serieslist.SeriesListFragment;

public class ScreensNavigator {

    private FragmentFrameHelper mFragmentFrameHelper;

    private final UtilsHelper mUtilsHelper;

    public ScreensNavigator(FragmentFrameHelper fragmentFrameHelper, UtilsHelper utilsHelper) {
        mFragmentFrameHelper = fragmentFrameHelper;
        mUtilsHelper = utilsHelper;
    }

    public void toMainActivity(FragmentActivity mFragmentActivity) {
        Intent i = new Intent(mFragmentActivity, MainActivity.class);
        mFragmentActivity.startActivity(i);
        mFragmentActivity.finish();
    }

    public void toSeriesList() {
        mFragmentFrameHelper.replaceFragmentAndClearBackstack(SeriesListFragment.newInstance());
    }

    public void toSeriesDetails(SeriesSchema series) {
        mFragmentFrameHelper.replaceFragment(SeriesDetailFragment.newInstance(series));
    }

    public void toEpisodeDetails(EpisodeSchema episode, Context context) {
        new EpisodeDetailsDialog(episode, context, mUtilsHelper).openEpisodeDetailsDialog();
    }

    public void navigateUp() {
        mFragmentFrameHelper.navigateUp();
    }
}
