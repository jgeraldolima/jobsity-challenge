package com.jgeraldo.jobsitytvserieschallenge.screens.seriesdetails;

import android.content.Context;

import com.jgeraldo.jobsitytvserieschallenge.networking.episode.EpisodeSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.screensnavigator.ScreensNavigator;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.snackbarhelper.SnackbarHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.serieslist.SeriesListController;
import com.jgeraldo.jobsitytvserieschallenge.series.FetchSeriesEpisodesUseCase;

import java.io.Serializable;
import java.util.List;

public class SeriesDetailController implements
        SeriesDetailsViewMvc.Listener,
        FetchSeriesEpisodesUseCase.Listener {

    private enum ScreenState {
        IDLE, FETCHING_EPISODES, EPISODES_LIST_SHOWN, NO_RESULTS, REQUEST_ERROR
    }

    private final FetchSeriesEpisodesUseCase mFetchEpisodesListUseCase;

    private final ScreensNavigator mScreensNavigator;

    private final SnackbarHelper mSnackbarHelper;

    private SeriesDetailsViewMvc mViewMvc;

    private ScreenState mScreenState = ScreenState.IDLE;

    public SeriesDetailController(FetchSeriesEpisodesUseCase fetchEpisodesListUseCase,
                                  ScreensNavigator screensNavigator, SnackbarHelper snackbarHelper) {
        mFetchEpisodesListUseCase = fetchEpisodesListUseCase;
        mScreensNavigator = screensNavigator;
        mSnackbarHelper = snackbarHelper;
    }

    public void bindView(SeriesDetailsViewMvc viewMvc) {
        mViewMvc = viewMvc;
    }

    public SavedState getSavedState() {
        return new SavedState(mScreenState);
    }

    public void restoreSavedState(SavedState savedState) {
        mScreenState = savedState.mScreenState;
    }

    public void onStart(SeriesSchema series) {
        mViewMvc.registerListener(this);
        mFetchEpisodesListUseCase.registerListener(this);

        mViewMvc.bindSeries(series);

        if (mScreenState != ScreenState.REQUEST_ERROR) {
            fetchEpisodesAndNotify(series.getId());
        }
    }

    public void onStop() {
        mViewMvc.unregisterListener(this);
        mFetchEpisodesListUseCase.unregisterListener(this);
    }

    private void fetchEpisodesAndNotify(int showId) {
        if (mScreenState != ScreenState.FETCHING_EPISODES) {
            mScreenState = ScreenState.FETCHING_EPISODES;
            mViewMvc.showProgressIndication();
            mFetchEpisodesListUseCase.fetchEpisodesAndNotify(showId);
        }
    }

    @Override
    public void onNavigateUpClicked() {
        mScreensNavigator.navigateUp();
    }

    @Override
    public void onEpisodeClicked(EpisodeSchema episode, Context context) {
        mScreensNavigator.toEpisodeDetails(episode, context);
    }

    @Override
    public void onEpisodesFetched(List<EpisodeSchema> episodeSchemas) {
        if (episodeSchemas != null && episodeSchemas.size() > 0) {
            mViewMvc.bindEpisodesList(episodeSchemas);
            mScreenState = ScreenState.EPISODES_LIST_SHOWN;
        } else {
            mScreenState = ScreenState.NO_RESULTS;
            mViewMvc.showNoEpisodesFound();
        }
    }

    @Override
    public void onEpisodesFetchFailed(String errorMessage) {
        mScreenState = ScreenState.REQUEST_ERROR;
        mViewMvc.hideProgressIndication();
        mSnackbarHelper.showSnackbar(mViewMvc.getRootView(), errorMessage);
    }

    public static class SavedState implements Serializable {
        private final ScreenState mScreenState;

        public SavedState(ScreenState screenState) {
            mScreenState = screenState;
        }
    }
}
