package com.jgeraldo.jobsitytvserieschallenge.screens.seriesdetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.jgeraldo.jobsitytvserieschallenge.common.constants.Constants;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.controllers.BaseFragment;

import static com.jgeraldo.jobsitytvserieschallenge.common.constants.Constants.EXTRA_SERIES_ITEM;

public class SeriesDetailFragment extends BaseFragment {

    private static final String SAVED_STATE_SCREEN_STATE = "SAVED_STATE_SCREEN_STATE";

    private static final String SAVED_STATE_CONTROLLER = "SAVED_STATE_CONTROLLER";

    private SeriesSchema seriesSchema;

    private SeriesDetailController mSeriesDetailController;

    public static Fragment newInstance(SeriesSchema series) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.EXTRA_SERIES_ITEM, series);
        SeriesDetailFragment fragment = new SeriesDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private enum ScreenState {
        IDLE, SERIES_EPISODES_SHOW, NO_EPISODES_FOUND, NETWORK_ERROR
    }

    private SeriesDetailsViewMvc mViewMvc;

    private ScreenState mScreenState = ScreenState.IDLE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mViewMvc = getCompositionRoot().getViewMvcFactory().getSeriesDetailViewMvc(container);

        mSeriesDetailController = getCompositionRoot().getSeriesDetailController();

        if (savedInstanceState != null) {
            mScreenState = (ScreenState) savedInstanceState.getSerializable(SAVED_STATE_SCREEN_STATE);
        }

        mSeriesDetailController.bindView(mViewMvc);

        return mViewMvc.getRootView();
    }

    private void restoreControllerState(Bundle savedInstanceState) {
        mSeriesDetailController.restoreSavedState(
                (SeriesDetailController.SavedState)
                        savedInstanceState.getSerializable(SAVED_STATE_CONTROLLER)
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        mSeriesDetailController.onStart(getSeries());
    }

    @Override
    public void onStop() {
        super.onStop();
        mSeriesDetailController.onStop();
    }

    private SeriesSchema getSeries() {
        Bundle args = getArguments();

        if (args != null && args.containsKey(EXTRA_SERIES_ITEM)) {
            seriesSchema = (SeriesSchema) args.getSerializable(EXTRA_SERIES_ITEM);
        }
        return seriesSchema;
    }
}