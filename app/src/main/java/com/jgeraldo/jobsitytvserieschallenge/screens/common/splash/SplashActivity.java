package com.jgeraldo.jobsitytvserieschallenge.screens.common.splash;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.jgeraldo.jobsitytvserieschallenge.screens.common.controllers.BaseActivity;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.fragmentframehelper.FragmentFrameWrapper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.screensnavigator.ScreensNavigator;

public class SplashActivity extends BaseActivity implements FragmentFrameWrapper {

    private SplashController mSplashController;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ScreensNavigator mScreensNavigator = getCompositionRoot().getScreensNavigator();
        SplashViewMvc mViewMvc = getCompositionRoot().getViewMvcFactory().getSplashViewMvc(null);
        mSplashController = getCompositionRoot().getSplashController();
        mSplashController.bindView(mViewMvc);

        setContentView(mViewMvc.getRootView());
    }

    @Override
    public void onStart() {
        super.onStart();
        mSplashController.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mSplashController.onStop();
    }

    @Override
    public FrameLayout getFragmentFrame() {
        return null;
    }
}