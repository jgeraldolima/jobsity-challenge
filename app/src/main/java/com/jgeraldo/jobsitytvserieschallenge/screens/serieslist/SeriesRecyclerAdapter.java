package com.jgeraldo.jobsitytvserieschallenge.screens.serieslist;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.ViewMvcFactory;
import com.jgeraldo.jobsitytvserieschallenge.screens.serieslist.serieslistitem.SeriesListItemViewMvc;

import java.util.ArrayList;
import java.util.List;

public class SeriesRecyclerAdapter extends RecyclerView.Adapter<SeriesRecyclerAdapter.MyViewHolder>
        implements SeriesListItemViewMvc.Listener {

    public interface Listener {
        void onLoadMore();
        void onSeriesClicked(SeriesSchema seriesSchema);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        private final SeriesListItemViewMvc mViewMvc;

        public MyViewHolder(SeriesListItemViewMvc viewMvc) {
            super(viewMvc.getRootView());
            mViewMvc = viewMvc;
        }
    }

    private final Listener mListener;

    private final ViewMvcFactory mViewMvcFactory;

    private List<SeriesSchema> mSeriesSchemas = new ArrayList<>();

    public SeriesRecyclerAdapter(Listener listener, ViewMvcFactory viewMvcFactory) {
        mListener = listener;
        mViewMvcFactory = viewMvcFactory;
    }

    public void bindSeriesList(List<SeriesSchema> seriesSchemas) {
        mSeriesSchemas = new ArrayList<>(seriesSchemas);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SeriesListItemViewMvc viewMvc = mViewMvcFactory.getSeriesListItemViewMvc(parent);
        viewMvc.registerListener(this);
        return new MyViewHolder(viewMvc);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.mViewMvc.bindSeries(mSeriesSchemas.get(position));

        if ((position >= getItemCount() - 3)) {
            mListener.onLoadMore();
        }
    }

    @Override
    public int getItemCount() {
        return mSeriesSchemas.size();
    }

    @Override
    public void onSeriesClicked(SeriesSchema seriesSchema) {
        mListener.onSeriesClicked(seriesSchema);
    }
}
