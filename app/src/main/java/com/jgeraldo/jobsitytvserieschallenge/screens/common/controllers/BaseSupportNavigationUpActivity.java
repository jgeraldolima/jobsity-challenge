package com.jgeraldo.jobsitytvserieschallenge.screens.common.controllers;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.jgeraldo.jobsitytvserieschallenge.R;

public class BaseSupportNavigationUpActivity extends BaseActivity {

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_item_detail);
        return navController.navigateUp() || super.onSupportNavigateUp();
    }

}
