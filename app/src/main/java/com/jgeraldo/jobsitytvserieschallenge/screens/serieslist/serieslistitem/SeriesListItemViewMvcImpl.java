package com.jgeraldo.jobsitytvserieschallenge.screens.serieslist.serieslistitem;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jgeraldo.jobsitytvserieschallenge.R;
import com.jgeraldo.jobsitytvserieschallenge.networking.common.PosterImageSchema;
import com.jgeraldo.jobsitytvserieschallenge.networking.series.SeriesSchema;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.utilshelper.UtilsHelper;
import com.jgeraldo.jobsitytvserieschallenge.screens.common.views.BaseObservableViewMvc;

import static com.jgeraldo.jobsitytvserieschallenge.common.constants.Constants.DASH;

public class SeriesListItemViewMvcImpl extends BaseObservableViewMvc<SeriesListItemViewMvc.Listener>
        implements SeriesListItemViewMvc {

    private final ImageView ivSeriesPoster;

    private final TextView tvSeriesName;

    private final TextView tvSeriesRating;

    private final TextView tvSeriesGenres;

    private SeriesSchema mSeriesSchema;

    private final UtilsHelper mUtilsHelper;

    public SeriesListItemViewMvcImpl(LayoutInflater inflater, @Nullable ViewGroup parent, UtilsHelper utilsHelper) {
        setRootView(inflater.inflate(R.layout.series_item_content, parent, false));

        mUtilsHelper = utilsHelper;

        ivSeriesPoster = findViewById(R.id.iv_series_poster);
        tvSeriesName = findViewById(R.id.tv_series_name);
        tvSeriesRating = findViewById(R.id.tv_series_rating);
        tvSeriesGenres = findViewById(R.id.tv_series_genres);

        getRootView().setOnClickListener(view -> {
            for (Listener listener : getListeners()) {
                listener.onSeriesClicked(mSeriesSchema);
            }
        });
    }

    @Override
    public void bindSeries(SeriesSchema seriesSchema) {
        mSeriesSchema = seriesSchema;

        PosterImageSchema poster = seriesSchema.getPosterImage();
        if (poster != null) {
            Glide.with(getContext())
                    .load(poster.getOriginalImageUrl())
                    .apply(new RequestOptions().error(R.drawable.ic_baseline_movie_24))
                    .into(ivSeriesPoster);
        }

        tvSeriesName.setText(seriesSchema.getName());

        Float rating = seriesSchema.getRating().getAverage();
        tvSeriesRating.setText(rating != null ? String.valueOf(rating) : DASH);

        tvSeriesGenres.setText(mUtilsHelper.getSeriesGenres(seriesSchema));
    }
}
