<div align="center">
    <img src="https://raw.githubusercontent.com/Jobsity/ReactChallenge/main/src/assets/jobsity_logo_small.png"/>
</div>

## Description

The main purpose of this project is to test what kind of solutions you can give to certain
day-to-day tasks and how well you can accomplish good practices. Do not aim to take the test in
the shortest possible time, but with the highest quality, you can achieve.

## Assignment

You will develop an application for listing TV series, using the API provided by the TVMaze
website. You can find the API [here](https://www.tvmaze.com/api).

## Mandatory features

* List all of the series contained in the API used by the paging scheme provided by the
API.
* Allow users to search series by name.
* The listing and search views must show at least the name and poster image of the
series.
* After clicking on a series, the application should show the details of the series, showing
the following information:
    * Name
    * Poster
    * Days and time during which the series airs
    * Genres
    * Summary
    * List of episodes separated by season
* After clicking on an episode, the application should show the episode’s information,
including:
    * Name
    * Number
    * Season
    * Summary
    * Image, if there is one

## Extra features / Relevant details

- Use of Retrofit + Okhttp
- Use of ButterKnife for components referencing (some classes, since I didn't have time to apply it after the request refactoring)
- Multiple languages Support (English and Portuguese)
- UI feedbacks for requests errors (using snackbar)
- Robust layout implementation using containers like CoordinatorLayout and ConstraintLayout
- Custom app icon
- Custom fonts and theme colors
- Custom splash screen
- Webpage URL shortcut for both series and episodes

## Architecture

For this project, it was used the MVC (Model View Controller) + Dependency Injection patterns in order to build a completly decoupled UI logic from business and more testable solution. Using this approcach, Activities and Fragments become Controllers and the Views becomes standalone classes. Also, it was used Observer and Hierarchy patterns to increase code reuse.

The basic project architecture implementation uses the following sctructure:


### *Dependency Injection*

Dependency injection may have two meanings: either a technique (constructor/field injection) for provide (inject) required project dependencies or simply an architectural pattern. In our case would be the second one, separation of concerns. We basically divide the application's logic into two separated sets, which are: 

- **Construction** set: classes that resolve dependencies and instantiate objects for the Functional Set, and in our case includes CompositionRoot, ControllerCompositionRoot and ViewMvcFactory
- **Functional** set: classes that encapsulate core application's functionality, it is, basically all the other classes


Getting into more details:

1. **CompositionRoot**: core component, it instantiates global objects for the application common functionalities set and expose them to the outside world;
2. **ActivityCompositionRoot**: encapsulates the core CompositionRoot and the Activity instance;
3. **ControllerCompositionRoot**: encapsulates the ActivityCompositionRoot and implement getters for common components like API, LayoutInflater, ViewMVCFactory, etc, in order to deliver a controller-like behavior for the activities;
4. **CustomApplication**: it overrides the default Application so we can use our CompositionRoot implementation to tie all up and enjoy our architecture;
5. **BaseActivity**: holds the ActivityCompositionRoot and ControllerCompositionRoot instances so the Activities implementations won't need to know what and how things are implemented on the higher levels;


### *MVC*

After all, the basic MVC structure implements the following pattern:

```
Activity/Fragments -> Controllers (MVCViewImpl, UI logic) -> Use case (service fetch) 
```
---

We can see below a better description for each relevant project component divided by package:

<details open>
  <summary><b>package: common</b></summary>
  
##### - *sub-package*: dependencyinjection

All classes responsible for providing the Dependecy Injection Architecture Pattern implementation.

##### - CustomApplication.java

It overrides the default Application so we can use our CompositionRoot implementation to tie all up and enjoy our architecture.

##### - BaseObservable.java

It implements the base methods to implement a observable class taking care of the current list of listeners and making sure it is unmodifiable, all behind the scenes.
</details>


<details open>
  <summary><b>package: networking</b></summary>

<br>
Everything related with external resources (API requests).

##### - *sub-package*: common

Classes that represents common API response data structures.

##### - TvMazeCloudApiInterface.java

It defines all used API services, exposing their endpoints, path and query parameters, etc.
</details>


<details open>
  <summary><b>package: screens</b></summary>

<br>
Everything related to UI components.

##### - *sub-package*: common

All reusable UI components, like activities, controllers, searchview, toolbar, etc.


##### - - *sub-package*: views


###### - ViewMvc.class

Parent interface that defines the main necessary View method: getRootView. All child classes need to use it to return their parent Android View component instances in order to allow the reusable UI components to manipulate it.

###### - BaseViewMvc.class

Implements the ViewMvc.class and uses the root view instance to implement more useful gets like getContext, getString, getResources, etc.

###### - ObservableViewMvc.class

Extends the ViewMvc.class to add the two necessary methods to define a Observable behavior on the classes that will implement it: registerListener and unregisterListener. 

###### - BaseObservableViewMvc.class

Parent class that extends BaseViewMvc.class and implements the ObservableViewMvc.class interface, adding a implementation for an unmodifiable set of listeners field that will be manipulated by the interface methods. This prevents the child classes to be responsible for this management.

---
##### - ViewMvcFactory.class

Implements a set of getters of MVC classes (screens and components) implementations instances by holding the reusable LayoutInflater (responsible for instantiate MVC views) and UtilsHelper instance.
</details>

<details open>
  <summary><b>package: series</b></summary>

All series API use cases implementations, responsible to fetch and process the service data and pass it back to the controllers using listeners.
</details>

# How to import

The project is a simple app, so we do not have any external dependencies or modules, just import it normally on Android Studio.